package com.learn.mentee;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class EventHandler implements RequestHandler<DynamodbEvent, String> {

    private final AmazonS3 s3;

    private final File htmlTemplateFile;

    public EventHandler() {
        Regions region = Regions.US_EAST_1;
        s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
        htmlTemplateFile = new File("template.html");
    }


    @Override
    public String handleRequest(DynamodbEvent dynamodbEvent, Context context) {
        String destBucketName = "mentee-bucket";
        String htmlString;
        StringBuilder stringBuilder = new StringBuilder();
        for (DynamodbEvent.DynamodbStreamRecord record : dynamodbEvent.getRecords()) {
            stringBuilder.append(record).append("\n");
        }
        try {
            String charsetName = "UTF-8";
            htmlString = FileUtils.readFileToString(htmlTemplateFile, charsetName);
            htmlString = htmlString.replace("$body", stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return "Fail";
        }
        InputStream inputStream = new ByteArrayInputStream(htmlString.getBytes(StandardCharsets.UTF_8));
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("text/html");
        PutObjectRequest putObjectRequest = new PutObjectRequest(destBucketName, "newTemplate.html", inputStream, objectMetadata);
        s3.putObject(putObjectRequest);
        return "Success";
    }
}
